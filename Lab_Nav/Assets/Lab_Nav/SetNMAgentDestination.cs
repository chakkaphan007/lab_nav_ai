﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UIElements;
using UnityEngine.AI;

[RequireComponent(typeof(AICharacterControl))]
public class SetNMAgentDestination : MonoBehaviour
{
    private AICharacterControl aiCharControl;
    [SerializeField]
    private float surfaceOffset = 0;
    // Start is called before the first frame update
    void Start()
    {
        aiCharControl = GetComponent<AICharacterControl>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown((int)MouseButton.LeftMouse))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) == true)
            {
                GameObject pickedTarget = new GameObject();
                pickedTarget.transform.position = hit.point + hit.normal * surfaceOffset;
                aiCharControl.SetTarget(pickedTarget.transform);
                Destroy(pickedTarget, 0.2f);
            }
        }
    }
}
