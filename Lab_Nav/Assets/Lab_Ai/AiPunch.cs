﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AiPunch : MonoBehaviour
{
    [SerializeField] TextMeshPro m_Object;
    public string currentState;
    public Transform Player;
    float MobdistanceRun = 1.0f;
    float outofview = 10.0f;
    private UnityEngine.AI.NavMeshAgent Mop;

    Animator anim;
    const string WALK = "Walk";
    const string IDLE = "Idle";
    const string PUNCH1 = "Punch1";

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        Mop = GetComponent<UnityEngine.AI.NavMeshAgent> ();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance (transform.position, Player.transform.position);
        if (distance > MobdistanceRun && distance < outofview)
        {
            transform.LookAt(Player);
            Mop.SetDestination(Player.transform.position);
            ChangeAnimationState(WALK);
            m_Object.text = WALK;
        }
        if (distance < MobdistanceRun) {
            transform.LookAt(Player);
            Vector3 dirToPlayer = transform.position - transform.position;
            Vector3 newPos = transform.position - dirToPlayer;
            Mop.SetDestination (newPos);
            ChangeAnimationState(PUNCH1);
            m_Object.text = PUNCH1;
        }
        if (distance > outofview) {
            Debug.Log("out");
            Vector3 dirToPlayer = transform.position - transform.position;
            Vector3 newPos = transform.position - dirToPlayer;
            Mop.SetDestination (newPos);
            ChangeAnimationState(IDLE);
            m_Object.text = IDLE;
        }
    }
    void ChangeAnimationState(string newState)
    {
        if(currentState == newState) return;
        anim.Play(newState);
        currentState = newState;
    }
}

