﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ai : MonoBehaviour
{
    [SerializeField] TextMeshPro m_Object;
    public string currentState;
    public Transform Player;
    float MobdistanceRun = 5.0f;
    float outofview = 10.0f;
    private UnityEngine.AI.NavMeshAgent Mop;

    Animator anim;
    const string WALK = "Walk";
    const string IDLE = "Idle";
    const string SHOOT = "Shoot";
    const string RUNSHOOT = "RunShoot";
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        Mop = GetComponent<UnityEngine.AI.NavMeshAgent> ();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance (transform.position, Player.transform.position);
        if (distance > MobdistanceRun && distance < outofview)
        {
            transform.LookAt(Player);
            transform.Rotate(0, 45, 0);
            Mop.SetDestination(Player.transform.position);
            ChangeAnimationState(WALK);
            m_Object.text = WALK;
        }
        if (distance < MobdistanceRun) {
            transform.LookAt(Player);
            transform.Rotate(0, 45, 0);
            Vector3 dirToPlayer = transform.position - transform.position;
            Vector3 newPos = transform.position - dirToPlayer;
            Mop.SetDestination (newPos);
            ChangeAnimationState(SHOOT);
            m_Object.text = SHOOT;
        }
        if (distance > outofview) {
            Debug.Log("out");
            Vector3 dirToPlayer = transform.position - transform.position;
            Vector3 newPos = transform.position - dirToPlayer;
            Mop.SetDestination (newPos);
            ChangeAnimationState(IDLE);
            m_Object.text = IDLE;
        }

    }
    void ChangeAnimationState(string newState)
    {
        if(currentState == newState) return;
        anim.Play(newState);
        currentState = newState;
    }
}

