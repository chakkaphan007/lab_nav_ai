﻿

using UnityEngine;

namespace Lab_Ai.FSM
{
    public class Idle1 : State
    {
        public ZombieAIFSM1 localFSM { get; set; }

        private float delayTime;
        
        public Idle1(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM1)fsm;

            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
        }

        public override void Enter()
        {
            localFSM.Anim.SetTrigger("IDLE");
            delayTime = Random.Range(2, 5);
            base.Enter();
        }

        public override void Update()
        {
            
            delayTime -= Time.deltaTime;
            if (delayTime <= 0)
            {
                //Prepare to go to the next state, Patrol
                localFSM.NextState = new Patrol1(localFSM);
                this.StateStage = StateEvent.EXIT;
            }
        }
        
        public override void Exit()
        {
            localFSM.Anim.ResetTrigger("IDLE");
            base.Enter();
        }
    }
}
