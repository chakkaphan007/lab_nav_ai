﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KS.AI.FSM
{
    public class FiniteStateMachine : MonoBehaviour
    {
        public State CurrentState { get; set; }
        public State NextState { get; set; }
        public void ProcessFSM()
        {
            if (CurrentState == null) return;
            switch (CurrentState.StateStage)
            {
                case StateEvent.ENTER:
                    CurrentState.Enter();
                    break;
                case StateEvent.UPDATE:
                    CurrentState.Update();
                    break;
                case StateEvent.EXIT:
                    CurrentState.Exit();
                    CurrentState = NextState;
                    break;
            }
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            ProcessFSM();
        }
    }
}
