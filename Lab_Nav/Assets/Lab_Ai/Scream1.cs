﻿using UnityEngine;

namespace Lab_Ai.FSM
{
    public class Scream1 : State
    {
        public ZombieAIFSM1 localFSM { get; set; }
        
        private float delayTime;
        
        public Scream1(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM1)fsm;

            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
        }

        public override void Enter()
        {
            localFSM.Anim.SetTrigger("SHOOT");
            delayTime = Random.Range(2, 5);
            base.Enter();
        }

        public override void Update()
        {
            delayTime -= Time.deltaTime;
            if (delayTime <= 0)
            {
                //Prepare to go to the next state, Patrol
                localFSM.NextState = new Idle1(localFSM);
                this.StateStage = StateEvent.EXIT;
            }
        }

        public override void Exit()
        {
            localFSM.Anim.ResetTrigger("SHOOT");
            base.Exit();
        }
        
    }
}
