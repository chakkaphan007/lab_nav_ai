﻿using UnityEngine;

namespace Lab_Ai.FSM
{
    public class RunChasing1 : State
    {
        public ZombieAIFSM1 localFSM { get; set; }
        
        public RunChasing1(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM1)fsm;
            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
            
        }

        public override void Enter()
        {
            localFSM.AIChar.SetTarget(localFSM.player);
            localFSM.Agent.SetDestination(localFSM.player.position);
            
            localFSM.Anim.SetTrigger("WALK");

            base.Enter();
        }

        public override void Update()
        {
            #region Chasing the player

            localFSM.Agent.SetDestination(localFSM.player.position);
            
            if (localFSM.Agent.remainingDistance > localFSM.Agent.stoppingDistance)
            {
                //Move the agent
                localFSM.ThirdPersonChar.Move(localFSM.Agent.desiredVelocity, false, false);
            }

            #endregion
            
            #region Checking if the player is escaped successfully/behind

            if (localFSM.IsPlayerBehind())
            {
                    FSM.NextState = new Idle1(FSM);
                    this.StateStage = StateEvent.EXIT;
            }

            #endregion
            
            
            if (localFSM.IsPlayerInAttackRange)
            {
                FSM.NextState = new Attack1(FSM);
                this.StateStage = StateEvent.EXIT;
            }
        }

        public override void Exit()
        {
            localFSM.Anim.ResetTrigger("WALK");

            localFSM.StopAINavigation();


            base.Exit();
        }
    }
}
