﻿using UnityEngine;

namespace Lab_Ai.FSM
{
    public class Attack1 : State
    {
        public ZombieAIFSM1 localFSM { get; set; }
        
        public Attack1(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM1)fsm;

            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
        }

        public override void Enter()
        {
            localFSM.Anim.SetTrigger("SHOOT");

            localFSM.StopAINavigation();

            base.Enter();
        }

        public override void Update()
        {
            if (!localFSM.IsPlayerInAttackRange)
            {
                if (localFSM.IsPlayerBehind())
                {
                    FSM.NextState = new Idle1(FSM);
                    this.StateStage = StateEvent.EXIT;
                }
                else
                {
                    FSM.NextState = new Attack1(FSM);
                    this.StateStage = StateEvent.EXIT;
                }
            }
        }
        
        public override void Exit()
        {
            localFSM.Anim.ResetTrigger("SHOOT");

            localFSM.StopAINavigation();


            base.Exit();
        }
    }
}
